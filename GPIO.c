/*****************************************************************************
 * gpio.c
 *****************************************************************************/

#include <sys/platform.h>
#include "adi_initialize.h"

#include "GPIO.h"

//int main(int argc, char *argv[]) {
//	logicLevel result;
//	uint16_t result2;
//	adi_initComponents();
//
//	pinMode(DS3, OUTPUT);
//	pinMode(DS4, OUTPUT);
//	pinMode(IO08, INPUT_PULLUP);
//	do {
//		digitalWrite(DS3, HIGH);
//		digitalWrite(DS3, LOW);
//		digitalWrite(DS4, HIGH);
//		digitalWrite(DS4, LOW);
//		result = digitalRead(IO08);
//	}while (1);
//
//	return 0;
//}

uint8_t gpioMemory[ADI_GPIO_MEMORY_SIZE];

pinMap SCL = { ADI_GPIO_PORT0, ADI_GPIO_PIN_4 };
pinMap SDA = { ADI_GPIO_PORT0, ADI_GPIO_PIN_5 };
pinMap SCLK = { ADI_GPIO_PORT0, ADI_GPIO_PIN_0 };
pinMap MISO = { ADI_GPIO_PORT0, ADI_GPIO_PIN_2 };
pinMap MOSI = { ADI_GPIO_PORT0, ADI_GPIO_PIN_1 };
pinMap CS = { ADI_GPIO_PORT1, ADI_GPIO_PIN_10 };
pinMap RDY = { ADI_GPIO_PORT1, ADI_GPIO_PIN_14 };
pinMap IO28 = { ADI_GPIO_PORT1, ADI_GPIO_PIN_12 };
pinMap IO08 = { ADI_GPIO_PORT0, ADI_GPIO_PIN_8 };
pinMap IO27 = { ADI_GPIO_PORT1, ADI_GPIO_PIN_11 };
pinMap IO33 = { ADI_GPIO_PORT2, ADI_GPIO_PIN_1 };
pinMap IO09 = { ADI_GPIO_PORT0, ADI_GPIO_PIN_9 };
pinMap IO13 = { ADI_GPIO_PORT0, ADI_GPIO_PIN_13 };
pinMap IO15 = { ADI_GPIO_PORT0, ADI_GPIO_PIN_15 };
pinMap TX = { ADI_GPIO_PORT0, ADI_GPIO_PIN_10 };
pinMap RX = { ADI_GPIO_PORT0, ADI_GPIO_PIN_11 };
pinMap AIN0 = { ADI_GPIO_PORT2, ADI_GPIO_PIN_3 };
pinMap AIN1 = { ADI_GPIO_PORT2, ADI_GPIO_PIN_4 };
pinMap AIN2 = { ADI_GPIO_PORT2, ADI_GPIO_PIN_5 };
pinMap AIN3 = { ADI_GPIO_PORT2, ADI_GPIO_PIN_6 };
pinMap AIN4 = { ADI_GPIO_PORT2, ADI_GPIO_PIN_7 };
pinMap AIN5 = { ADI_GPIO_PORT2, ADI_GPIO_PIN_8 };
pinMap IO16 = { ADI_GPIO_PORT1, ADI_GPIO_PIN_0 };
pinMap IO12 = { ADI_GPIO_PORT0, ADI_GPIO_PIN_12 };
pinMap DS4 = { ADI_GPIO_PORT1, ADI_GPIO_PIN_15 };
pinMap DS3 = { ADI_GPIO_PORT2, ADI_GPIO_PIN_0 };

ADI_GPIO_RESULT gpioSetup() {
	return adi_gpio_Init(gpioMemory, ADI_GPIO_MEMORY_SIZE);
}

ADI_GPIO_RESULT pinMode(pinMap pm, mode m) {
	if (m == INPUT) {
	return adi_gpio_InputEnable(pm.port, pm.pin, true);
} else if (m == OUTPUT) {
return adi_gpio_OutputEnable(pm.port, pm.pin, true);
} else if (m == INPUT_PULLUP) {
ADI_GPIO_RESULT result;
if (ADI_GPIO_SUCCESS != (result = adi_gpio_InputEnable(pm.port, pm.pin, true))) {
	return result;
}
if (ADI_GPIO_SUCCESS != (result = adi_gpio_PullUpEnable(pm.port, pm.pin, true))) {
	return result;
}
}
return 0;
}

ADI_GPIO_RESULT digitalWrite(pinMap pm, logicLevel b) {
if (b) {
return adi_gpio_SetHigh(pm.port, pm.pin);
} else {
return adi_gpio_SetLow(pm.port, pm.pin);
}
return 0;
}

logicLevel digitalRead(pinMap pm) {
uint16_t val;
adi_gpio_GetData(pm.port, pm.pin, &val);
return val ? HIGH : LOW;
}

ADI_GPIO_RESULT attachInterrupt(pinMap pm, ADI_CALLBACK const cb, Imode im,
ADI_GPIO_IRQ gp) {
ADI_GPIO_RESULT result;
if (ADI_GPIO_SUCCESS != (result = adi_gpio_SetGroupInterruptPins(pm.port, gp , pm.pin))) {
return result;
}
if (ADI_GPIO_SUCCESS != (result = adi_gpio_GroupInterruptPolarityEnable(pm.port, pm.pin, im))) {
return result;
}
if (ADI_GPIO_SUCCESS
!= (result = adi_gpio_RegisterCallback(gp, cb, (void*) gp))) {
return result;
}

return ADI_GPIO_SUCCESS;
}

ADI_GPIO_RESULT setupInterrupt(pinMap pm, ADI_CALLBACK const cb, Imode im,
ADI_GPIO_IRQ gp) {
ADI_GPIO_RESULT result;
if (ADI_GPIO_SUCCESS != (result = adi_gpio_SetGroupInterruptPins(pm.port, gp , pm.pin))) {
return result;
}
if (ADI_GPIO_SUCCESS != (result = adi_gpio_GroupInterruptPolarityEnable(pm.port, pm.pin, im))) {
return result;
}

return ADI_GPIO_SUCCESS;
}

ADI_GPIO_RESULT analogRead(pinMap pm, uint16_t *val) {
	if (pm.port == ADI_GPIO_PORT2) {
		if(pm.pin == ADI_GPIO_PIN_3 ||pm.pin == ADI_GPIO_PIN_4 ||pm.pin == ADI_GPIO_PIN_5 ||pm.pin == ADI_GPIO_PIN_6 ||pm.pin == ADI_GPIO_PIN_7 ||pm.pin == ADI_GPIO_PIN_8){
			return adi_gpio_GetData(pm.port, pm.pin, val);
		} else {
			return ADI_GPIO_INVALID_PINS;
		}
	}else{
		return ADI_GPIO_INVALID_PORT;
	}
} // Ainda n�o funciona
