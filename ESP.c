#include"ESP.h"

//char* strcat(char* destination, const char* source)
//{
//	int i, j;
//
//	// move to the end of destination string
//	for (i = 0; destination[i] != '\0'; i++);
//
//	// i now points to terminating null character in destination
//
//	// Appends characters of source to the destination string
//	for (j = 0; source[j] != '\0'; j++)
//		destination[i + j] = source[j];
//
//	// null terminate destination string
//	destination[i + j] = '\0';
//
//	// destination is returned by standard strcat()
//	return destination;
//}


void testStrcat(){
	char teste[10] = "oi";
	char ola[5] = "ola";
	char * concat;

	concat = strcat(teste, ola);
	uartSerial2();
	derlay(500);
	UART_WriteString((uint8_t *) concat);
	clearUartRx();

}

void sendData(){
	char strhost[14] = "192.168.1.117";
	char strurl[25] = "/Esp8266/enviardatos.php";
	char port[3] = "80";
	char postData[255];
	char start[255];
	char send[255];
	int chipid = 1;
	int temperatura = 24;
	int umidade = 12;

	//sprintf(postData,"POST %s HTTP/1.1\r\nHost: %s\r\nAccept: */**\r\nContent-Length: %d\r\nContent-Type: application/x-www-form-urlencoded\r\n\r\n%s\r\n",strurl,strhost,strlen(dados),dados);
	sprintf(postData,"GET http://192.168.1.117:80/Esp8266/enviardatos.php?chipid=%d&temperatura=%d&umidade=%d HTTP/1.0\r\n\r\n\r\n",chipid,temperatura,umidade);
	//startTCP

	sprintf(start,"AT+CIPSTART=\"TCP\",\"%s\",%s\r\n",strhost,port);

	uartSerial1();
    derlay(2000);
    clearUartRx();
    UART_WriteString((uint8_t *) start);
	uartSerial2();
	derlay(500);
	UART_WriteString((uint8_t *) uart_rx_buffer);
	derlay(500);

	sprintf(send, "AT+CIPSEND=%d\r\n",strlen(postData));
	uartSerial1();
	derlay(2000);
	clearUartRx();
	UART_WriteString((uint8_t *) send);
	uartSerial2();
	derlay(500);
	UART_WriteString((uint8_t *) uart_rx_buffer);
	derlay(20000);

	uartSerial1();
	derlay(2000);
	clearUartRx();
	UART_WriteString((uint8_t *) postData);
	uartSerial2();
	derlay(500);
	UART_WriteString((uint8_t *) uart_rx_buffer);
	derlay(2000);

////	char lenPost[5];
////	aux = strlen(postData);
////
////	for(int i =0;aux > 0;i++){
////		lenPost[i] = aux%10;
////		aux = aux/10;
////	}
////
////	uartSerial1();
////	char * send;
////	send = strcat("AT+CIPSEND=4,", lenPost);
////	send = strcat(send,"\r\n");
////	derlay(2000);
////	clearUartRx();
////	UART_WriteString((uint8_t *) send);
////	uartSerial2();
////	derlay(500);
////	UART_WriteString((uint8_t *) uart_rx_buffer);
////	derlay(2000);
////
////	uartSerial1();
////	clearUartRx();
////	UART_WriteString((uint8_t *) postData);
////	uartSerial2();
////	derlay(500);
////	UART_WriteString((uint8_t *) uart_rx_buffer);
////	derlay(2000);
////
//
	uartSerial1();
	clearUartRx();
	UART_WriteString((uint8_t *) "AT+CIPCLOSE\r\n");
	uartSerial2();
	derlay(500);
	UART_WriteString((uint8_t *) uart_rx_buffer);
	derlay(2000);

}

void createHotspot(){
	uartSerial1();
	derlay(500);
	clearUartRx();
	UART_WriteString((uint8_t *) "AT+CWMODE=2\r\n");
	uartSerial2();
	derlay(500);
	UART_WriteString((uint8_t *) uart_rx_buffer);
	clearUartRx();
}


void connect(char * ssid, char* pasw){
	char wifi[255];
	uartSerial1();
    derlay(2000);
    clearUartRx();
    UART_WriteString((uint8_t *) "AT+CWMODE=1\r\n");
    uartSerial2();
    derlay(2000);
    UART_WriteString((uint8_t *) uart_rx_buffer);
    clearUartRx();
    uartSerial1();
    derlay(500);
    sprintf(wifi,"AT+CWJAP=\"%s\",\"%s\"\r\n",ssid,pasw);
    UART_WriteString((uint8_t *) wifi);
    uartSerial2();
    derlay(2000);
    UART_WriteString((uint8_t *) uart_rx_buffer);
    clearUartRx();
 }

void SetMultiple() {
	uartSerial1();
	derlay(500);
    UART_WriteString((uint8_t *) "AT+CIPMUX=0\r\n");
    derlay(2000);
    uartSerial2();
    UART_WriteString((uint8_t *) uart_rx_buffer);
    clearUartRx();
}

void AddEOL(){
		uart_rx_buffer[uart_rcnt++] = '\r';
		uart_rx_buffer[uart_rcnt++] = '\n';
		uart_rx_buffer[uart_rcnt] = '\0';
}

int hasOK(){
	for(int i=0; i< uart_rcnt; i++){
		if(uart_rx_buffer[i] == 'O'){
			if(uart_rx_buffer[i+1] == 'K'){
					return i;
			}
		}
	}
	return -1;
}

void resetEsp(){
	uartSerial1();
	UART_WriteString((uint8_t *) "AT+RST\r\n");
	derlay(2000);
	uartSerial2();
	UART_WriteString((uint8_t *) uart_rx_buffer);
	clearUartRx();
}

void getIP(){
	uartSerial1();
	derlay(100);
	UART_WriteString((uint8_t *) "AT+CIFSR\r\n");
	uartSerial2();
	derlay(500);
	UART_WriteString((uint8_t *) uart_rx_buffer);
	clearUartRx();
}
